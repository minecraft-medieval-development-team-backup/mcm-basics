package com.mcm.basics.inventorysListeners;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mcm.core.Main;
import com.mcm.core.cache.OnlineCount;
import com.mcm.core.serverswitch.PlayerSwitch;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryPlay implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String uuid = player.getUniqueId().toString();

        if (event.getView().getTitle().equalsIgnoreCase("Survival: ")) {
            event.setCancelled(true);

            int slot = event.getSlot();

            int survivals = 5;
            if (slot == 11) connect(player, uuid, "survival-1");
            if (slot == 12) connect(player, uuid, "survival-2");
            if (slot == 13) connect(player, uuid, "survival-3");
            if (slot == 14) connect(player, uuid, "survival-4");
            if (slot == 15) connect(player, uuid, "survival-5");
            if (slot == 31) {
                for (int i = 1; i <= survivals; i++) {
                    if (OnlineCount.get("survival-" + i) != null && OnlineCount.get("survival-" + i).getCount() < 80) {
                        connect(player, uuid, "survival-" + i);
                    }
                }
            }
        }
    }

    private static void connect(Player player, String uuid, String server_name) {
        if (!server_name.equals(Main.server_name)) {
            try {
                boolean sended = PlayerSwitch.sendData(player, server_name);

                if (sended == true) {
                    ByteArrayDataOutput out = ByteStreams.newDataOutput();
                    out.writeUTF("Connect");
                    out.writeUTF(server_name);

                    player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());
                    player.sendMessage(Main.getTradution("wDcuNh4VjRY#k5c", uuid) + server_name);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                }
            } catch (Exception e) {
                if (OnlineCount.get(server_name) == null) {
                    player.sendMessage(Main.getTradution("yKx%z3mb5xnVVGs", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                } else {
                    player.sendMessage(Main.getTradution("4$DSeDuDTnzAhy&", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        } else {
            player.sendMessage(Main.getTradution("D$nS83k4#*bYAPc", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }
}
