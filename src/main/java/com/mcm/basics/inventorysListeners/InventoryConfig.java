package com.mcm.basics.inventorysListeners;

import com.mcm.core.Main;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryConfig implements Listener {

    @EventHandler
    public void onInteractInv(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        if (event.getClickedInventory() != null && event.getView().getTitle().equalsIgnoreCase(Main.getTradution("yrK8Ydy%V5#wFw$", uuid))) {
            event.setCancelled(true);

            com.mcm.core.database.ConfigDb configDb = com.mcm.core.database.ConfigDb.get(uuid);

            if (event.getSlot() == 25) {
                if (tag.equals("membro")) {
                    player.sendMessage(Main.getTradution("BBHB@Z3?Dcjh!Tw", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (configDb.getAdverts() == true) {
                    configDb.setAdverts(false);
                    player.sendMessage(Main.getTradution("qkW@T3Pe2XRpn3n", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                } else {
                    configDb.setAdverts(true);
                    player.sendMessage(Main.getTradution("$jkG68p!zZpJFwY", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 24) {
                if (tag.equals("membro")) {
                    player.sendMessage(Main.getTradution("BBHB@Z3?Dcjh!Tw", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (configDb.getInvisible() == true) {
                    configDb.setInvisible(false);
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        player.hidePlayer(Main.plugin, target);
                    }
                    player.sendMessage(Main.getTradution("8aW#g6da%M$MR?u", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                } else {
                    configDb.setInvisible(true);
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        player.showPlayer(Main.plugin, target);
                    }
                    player.sendMessage(Main.getTradution("BQhHDeku&8kwN$Z", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 23) {
                if (tag.equals("membro")) {
                    player.sendMessage(Main.getTradution("BBHB@Z3?Dcjh!Tw", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (configDb.getFly() == true) {
                    configDb.setFly(false);
                    player.setAllowFlight(false);
                    player.sendMessage(Main.getTradution("9Hsh$3Y7f?k#e@M", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                } else {
                    configDb.setFly(true);
                    player.setAllowFlight(true);
                    player.sendMessage(Main.getTradution("7hRUmHD5ca5u#PU", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 22) {
                if (configDb.getTeleports() == true) {
                    configDb.setTeleports(false);
                    player.sendMessage(Main.getTradution("qZ!xcW@rMj6ANg$", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                } else {
                    configDb.setTeleports(true);
                    player.sendMessage(Main.getTradution("$JqcA5@EBR3sEk*", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 21) {
                if (configDb.getDeposits() == true) {
                    configDb.setDeposits(false);
                    player.sendMessage(Main.getTradution("a%Fm?4Vmpm2%m7N", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                } else {
                    configDb.setDeposits(true);
                    player.sendMessage(Main.getTradution("r6g@$E#cEN&$DXh", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 20) {
                if (configDb.getChatGlobal() == true) {
                    configDb.setChatGlobal(false);
                    player.sendMessage(Main.getTradution("GUD#$u4RpWgh8ZK", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                } else {
                    configDb.setChatGlobal(true);
                    player.sendMessage(Main.getTradution("7Cdzab?@&nJmsdR", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                }
            }

            if (event.getSlot() == 19) {
                if (configDb.getChatLocal() == true) {
                    configDb.setChatLocal(false);
                    player.sendMessage(Main.getTradution("vAj*8mk4UcDGF3m", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                           player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                } else {
                    configDb.setChatLocal(true);
                    player.sendMessage(Main.getTradution("w#hPy2*9DG38?!C", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.openInventory(com.mcm.basics.managers.InventoryConfig.inv(uuid));
                        }
                    }, 5L);
                }
            }
        }
    }
}
