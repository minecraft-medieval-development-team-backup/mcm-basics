package com.mcm.basics;

import com.mcm.basics.inventorysListeners.InventoryConfig;
import com.mcm.basics.inventorysListeners.InventorySpawn;
import com.mcm.basics.listeners.*;
import com.mcm.basics.inventorysListeners.InventoryPlay;
import org.bukkit.Bukkit;

public class RegisterListeners {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new PlayerLeftEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerDamageEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerTeleportEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerDeathEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new InventoryConfig(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new InventorySpawn(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new InventoryPlay(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerJoinEvents(), Main.plugin);
    }
}
