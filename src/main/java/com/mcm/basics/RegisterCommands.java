package com.mcm.basics;

import com.mcm.basics.commands.*;

public class RegisterCommands {

    public static void register() {
        Main.instance.getCommand("tag").setExecutor(new CommandTag());
        Main.instance.getCommand("moeda").setExecutor(new CommandCoins());
        Main.instance.getCommand("coins").setExecutor(new CommandCoins());
        Main.instance.getCommand("voar").setExecutor(new CommandFly());
        Main.instance.getCommand("fly").setExecutor(new CommandFly());
        Main.instance.getCommand("repair").setExecutor(new CommandRepair());
        Main.instance.getCommand("reparar").setExecutor(new CommandRepair());
        Main.instance.getCommand("clear").setExecutor(new CommandClear());
        Main.instance.getCommand("clearinv").setExecutor(new CommandClear());
        Main.instance.getCommand("limparinv").setExecutor(new CommandClear());
        Main.instance.getCommand("weather").setExecutor(new CommandWeather());
        Main.instance.getCommand("clima").setExecutor(new CommandWeather());
        Main.instance.getCommand("time").setExecutor(new CommandTime());
        Main.instance.getCommand("tempo").setExecutor(new CommandTime());
        Main.instance.getCommand("echest").setExecutor(new CommandEchest());
        Main.instance.getCommand("enderchest").setExecutor(new CommandEchest());
        Main.instance.getCommand("kickall").setExecutor(new CommandKickall());
        Main.instance.getCommand("speed").setExecutor(new CommandSpeed());
        Main.instance.getCommand("velocidade").setExecutor(new CommandSpeed());
        Main.instance.getCommand("suicide").setExecutor(new CommandSuicide());
        Main.instance.getCommand("suicidar").setExecutor(new CommandSuicide());
        Main.instance.getCommand("topo").setExecutor(new CommandTop());
        Main.instance.getCommand("top").setExecutor(new CommandTop());
        Main.instance.getCommand("gamemode").setExecutor(new CommandGamemode());
        Main.instance.getCommand("gm").setExecutor(new CommandGamemode());
        Main.instance.getCommand("compactar").setExecutor(new CommandCompactar());
        Main.instance.getCommand("compact").setExecutor(new CommandCompactar());
        Main.instance.getCommand("hat").setExecutor(new CommandHat());
        Main.instance.getCommand("cabeca").setExecutor(new CommandHat());
        Main.instance.getCommand("cabeça").setExecutor(new CommandHat());
        Main.instance.getCommand("head").setExecutor(new CommandHat());
        Main.instance.getCommand("skull").setExecutor(new CommandHat());
        Main.instance.getCommand("invsee").setExecutor(new CommandInvsee());
        Main.instance.getCommand("kill").setExecutor(new CommandKill());
        Main.instance.getCommand("matar").setExecutor(new CommandKill());
        Main.instance.getCommand("jump").setExecutor(new CommandJump());
        Main.instance.getCommand("pular").setExecutor(new CommandJump());
        Main.instance.getCommand("vanish").setExecutor(new CommandVanish());
        Main.instance.getCommand("v").setExecutor(new CommandVanish());
        Main.instance.getCommand("god").setExecutor(new CommandGod());
        Main.instance.getCommand("deus").setExecutor(new CommandGod());
        Main.instance.getCommand("heal").setExecutor(new CommandHeal());
        Main.instance.getCommand("rocket").setExecutor(new CommandRocket());
        Main.instance.getCommand("foguete").setExecutor(new CommandRocket());
        Main.instance.getCommand("spawn").setExecutor(new CommandSpawn());
        Main.instance.getCommand("setspawn").setExecutor(new CommandSpawn());
        Main.instance.getCommand("enchant").setExecutor(new CommandEnchant());
        Main.instance.getCommand("encantar").setExecutor(new CommandEnchant());
        Main.instance.getCommand("encantamentos").setExecutor(new CommandEnchant());
        Main.instance.getCommand("back").setExecutor(new CommandBack());
        Main.instance.getCommand("voltar").setExecutor(new CommandBack());
        Main.instance.getCommand("config").setExecutor(new CommandConfig());
        Main.instance.getCommand("tp").setExecutor(new CommandTeleport());
        Main.instance.getCommand("tpa").setExecutor(new CommandTeleport());
        Main.instance.getCommand("tppos").setExecutor(new CommandTeleport());
        Main.instance.getCommand("tphere").setExecutor(new CommandTeleport());
        Main.instance.getCommand("tpall").setExecutor(new CommandTeleport());
        Main.instance.getCommand("construir").setExecutor(new CommandConstruir());
        Main.instance.getCommand("builds").setExecutor(new CommandConstruir());
        Main.instance.getCommand("build").setExecutor(new CommandConstruir());
        Main.instance.getCommand("play").setExecutor(new CommandPlay());
        Main.instance.getCommand("jogar").setExecutor(new CommandPlay());
        Main.instance.getCommand("survival").setExecutor(new CommandPlay());
    }
}
