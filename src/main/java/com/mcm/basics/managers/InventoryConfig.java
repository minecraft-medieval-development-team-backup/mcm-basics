package com.mcm.basics.managers;

import com.mcm.core.Main;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import net.minecraft.server.v1_15_R1.NBTTagList;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class InventoryConfig {

    public static Inventory inv(String uuid) {
        String[] loreOn = {" ", Main.getTradution("wNHv*nBP#v?P2F%", uuid), " "};
        String[] loreOff = {" ", Main.getTradution("A!kMDaM79%WAr@z", uuid), " "};

        ItemStack chatLocalOn = getItem(Material.PAPER, (short) 0, Main.getTradution("K?e%Xs9fF#J43UX", uuid), loreOn, false);
        ItemStack chatLocalOff = getItem(Material.PAPER, (short) 0, Main.getTradution("K?e%Xs9fF#J43UX", uuid), loreOff, false);
        ItemStack chatGlobalOn = getItem(Material.MAP, (short) 0, Main.getTradution("K46%G2bfYV#!w$V", uuid), loreOn, false);
        ItemStack chatGlobalOff = getItem(Material.MAP, (short) 0, Main.getTradution("K46%G2bfYV#!w$V", uuid), loreOff, false);
        ItemStack depositsOn = getItem(Material.GOLD_INGOT, (short) 0, Main.getTradution("7rj@F!?fKh!t9Ws", uuid), loreOn, false);
        ItemStack depositsOff = getItem(Material.GOLD_INGOT, (short) 0, Main.getTradution("7rj@F!?fKh!t9Ws", uuid), loreOff, false);
        ItemStack teleportsOn = getItem(Material.ENDER_PEARL, (short) 0, Main.getTradution("6Pedvga7a6C?r2U", uuid), loreOn, false);
        ItemStack teleportsOff = getItem(Material.ENDER_PEARL, (short) 0, Main.getTradution("6Pedvga7a6C?r2U", uuid), loreOff, false);
        ItemStack flyOn = getItem(Material.FEATHER, (short) 0, Main.getTradution("V4TY52gf!X5Qc9A", uuid), loreOn, false);
        ItemStack flyOff = getItem(Material.FEATHER, (short) 0, Main.getTradution("V4TY52gf!X5Qc9A", uuid), loreOff, false);
        ItemStack invisibleOn = getItem(Material.POTION, (short) 0, Main.getTradution("tP4TzN&4y#SSVVk", uuid), loreOn, false);
        ItemStack invisibleOff = getItem(Material.POTION, (short) 0, Main.getTradution("tP4TzN&4y#SSVVk", uuid), loreOff, false);
        ItemStack advertsOn = getItem(Material.DIAMOND, (short) 0, Main.getTradution("ZJ5V8r#P*DjxReQ", uuid), loreOn, false);
        ItemStack advertsOff = getItem(Material.DIAMOND, (short) 0, Main.getTradution("ZJ5V8r#P*DjxReQ", uuid), loreOff, false);
        ItemStack on = getItem(Material.LIME_STAINED_GLASS, (short) 5, Main.getTradution("wNHv*nBP#v?P2F%", uuid), null, true);
        ItemStack off = getItem(Material.RED_STAINED_GLASS, (short) 14, Main.getTradution("A!kMDaM79%WAr@z", uuid), null, true);

        Inventory inventory = Bukkit.createInventory(null, 4 * 9, Main.getTradution("yrK8Ydy%V5#wFw$", uuid));

        com.mcm.core.database.ConfigDb configDb = com.mcm.core.database.ConfigDb.get(uuid);
        if (configDb.getChatLocal() == true) inventory.setItem(10, chatLocalOn);
        else inventory.setItem(10, chatLocalOff);
        if (configDb.getChatGlobal() == true) inventory.setItem(11, chatGlobalOn);
        else inventory.setItem(11, chatGlobalOff);
        if (configDb.getDeposits() == true) inventory.setItem(12, depositsOn);
        else inventory.setItem(12, depositsOff);
        if (configDb.getTeleports() == true) inventory.setItem(13, teleportsOn);
        else inventory.setItem(13, teleportsOff);
        if (configDb.getFly() == true) inventory.setItem(14, flyOn);
        else inventory.setItem(14, flyOff);
        if (configDb.getInvisible() == true) inventory.setItem(15, invisibleOn);
        else inventory.setItem(15, invisibleOff);
        if (configDb.getAdverts() == true) inventory.setItem(16, advertsOn);
        else inventory.setItem(16, advertsOff);

        if (configDb.getChatLocal() == true) inventory.setItem(19, on);
        else inventory.setItem(19, off);
        if (configDb.getChatGlobal() == true) inventory.setItem(20, on);
        else inventory.setItem(20, off);
        if (configDb.getDeposits() == true) inventory.setItem(21, on);
        else inventory.setItem(21, off);
        if (configDb.getTeleports() == true) inventory.setItem(22, on);
        else inventory.setItem(22, off);
        if (configDb.getFly() == true) inventory.setItem(23, on);
        else inventory.setItem(23, off);
        if (configDb.getInvisible() == true) inventory.setItem(24, on);
        else inventory.setItem(24, off);
        if (configDb.getAdverts() == true) inventory.setItem(25, on);
        else inventory.setItem(25, off);

        return inventory;
    }

    private static ItemStack getItem(Material material, short id, String displayName, String[] description, boolean addGlow) {
        ItemStack item = null;

        if (id != 0) {
            item = new ItemStack(material, 1, id);
        } else item = new ItemStack(material);
        ItemMeta metaItem = item.getItemMeta();
        metaItem.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaItem.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaItem.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaItem.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaItem.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaItem.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        metaItem.setDisplayName(displayName);

        if (description != null) {
            ArrayList<String> lore = new ArrayList<>();
            for (String desc : description) {
                lore.add(desc);
            }
            metaItem.setLore(lore);
        }

        item.setItemMeta(metaItem);

        if (addGlow == true) return addGlow(item);
        else return item;
    }

    public static ItemStack addGlow(ItemStack item) {
        net.minecraft.server.v1_15_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();
        NBTTagList ench = new NBTTagList();
        tag.set("ench", ench);
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }
}
