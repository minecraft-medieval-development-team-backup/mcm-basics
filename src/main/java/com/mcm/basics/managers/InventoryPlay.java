package com.mcm.basics.managers;

import com.mcm.core.Main;
import com.mcm.core.cache.OnlineCount;
import com.mcm.core.utils.EnchantmentGlow;
import com.mcm.core.utils.RemoveAllFlags;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class InventoryPlay {

    public static Inventory get(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Survival: ");

        inventory.setItem(11, createSpawnItem(uuid, Material.DIRT, "survival-1"));
        inventory.setItem(12, createSpawnItem(uuid, Material.GRASS_BLOCK, "survival-2"));
        inventory.setItem(13, createSpawnItem(uuid, Material.STONE, "survival-3"));
        inventory.setItem(14, createSpawnItem(uuid, Material.COBBLESTONE, "survival-4"));
        inventory.setItem(15, createSpawnItem(uuid, Material.OAK_PLANKS, "survival-5"));

        ItemStack random = new ItemStack(Material.FLOWER_BANNER_PATTERN);
        ItemMeta metaRandom = random.getItemMeta();
        metaRandom.setDisplayName(Main.getTradution("BGMwv33PzVgRF8&", uuid));
        ArrayList loreRandom = new ArrayList();
        loreRandom.add(Main.getTradution("&8C*KhPH7!WYEwn", uuid));
        loreRandom.add(" ");
        loreRandom.add(Main.getTradution("Gu5XBT&6YBa88Ph", uuid));
        metaRandom.setLore(loreRandom);
        random.setItemMeta(metaRandom);

        ItemStack back = new ItemStack(Material.ARROW);
        ItemMeta metaBack = back.getItemMeta();
        metaBack.setDisplayName(Main.getTradution("NF@p4kK&Pk5Z$V%", uuid));
        ArrayList loreBack = new ArrayList();
        loreBack.add(" ");
        loreBack.add(Main.getTradution("%vvkfK8SgF5Vdx8", uuid));
        metaBack.setLore(loreBack);
        back.setItemMeta(metaBack);

        ItemStack next = new ItemStack(Material.ARROW);
        ItemMeta metaNext = next.getItemMeta();
        metaNext.setDisplayName(Main.getTradution("3FjrEcaT&dJq8!G", uuid));
        ArrayList loreNext = new ArrayList();
        loreNext.add(" ");
        loreNext.add(Main.getTradution("Jv9rDC7@?p2YMZ3", uuid));
        metaNext.setLore(loreNext);
        next.setItemMeta(metaNext);

        inventory.setItem(31, RemoveAllFlags.remove(random));
        inventory.setItem(27, RemoveAllFlags.remove(back));
        inventory.setItem(35, RemoveAllFlags.remove(next));

        return inventory;
    }

    private static ItemStack createSpawnItem(String uuid, Material material, String server_name) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.GREEN + server_name.substring(0, 1).toUpperCase() + server_name.substring(1));
        ArrayList lore = new ArrayList();
        lore.add(Main.getTradution("j4cpYzXhaS$GD?c", uuid) + getServerStatus(server_name));
        if (!server_name.equals(com.mcm.core.Main.server_name)) {
            if (OnlineCount.get(server_name) == null) lore.add(Main.getTradution("jY$E4Q!gkt4NY@f", uuid) + ChatColor.GREEN + "0/80"); else lore.add(Main.getTradution("jY$E4Q!gkt4NY@f", uuid) + ChatColor.GREEN + OnlineCount.get(server_name).getCount() + "/80");
        } else lore.add(Main.getTradution("jY$E4Q!gkt4NY@f", uuid) + ChatColor.GREEN + Bukkit.getOnlinePlayers().size() + "/80");
        lore.add(" ");
        lore.add(Main.getTradution("8d92m@rUW$D5dbX", uuid));
        lore.add(" ");
        if (server_name.equals(com.mcm.core.Main.server_name)) {
            lore.add(Main.getTradution("Xk*S72CU8kjvTY?", uuid));
        } else lore.add(Main.getTradution("Gu5XBT&6YBa88Ph", uuid));
        meta.setLore(lore);
        item.setItemMeta(meta);

        if (server_name.equals(com.mcm.core.Main.server_name)) return EnchantmentGlow.addGlow(RemoveAllFlags.remove(item)); else return RemoveAllFlags.remove(item);
    }

    private static String getServerStatus(String server) {
        if (Main.server_name.equals(server)) return ChatColor.GREEN + "Online.";
        if (OnlineCount.get(server) == null) {
            return ChatColor.RED + "Offline.";
        } else return ChatColor.GREEN + "Online.";
    }
}
