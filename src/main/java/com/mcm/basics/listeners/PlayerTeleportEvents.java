package com.mcm.basics.listeners;

import com.mcm.basics.cache.LastPosition;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

public class PlayerTeleportEvents implements Listener {

    @EventHandler
    public void onTeleport(PlayerTeleportEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        String location = player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch();

        if (LastPosition.get(uuid) == null) {
            new LastPosition(uuid, location).insert();
        } else LastPosition.get(uuid).setLocation(location);
    }
}
