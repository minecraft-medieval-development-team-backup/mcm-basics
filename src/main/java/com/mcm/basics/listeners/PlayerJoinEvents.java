package com.mcm.basics.listeners;

import com.mcm.basics.Main;
import com.mcm.core.Redis;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinEvents implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
            String tp = Redis.localhost.get(uuid + "tp");
            if (tp != null) {
                Player target = Bukkit.getPlayerExact(tp);
                player.teleport(target);
                Redis.localhost.del(uuid + "tp");
            }

            String tpa = Redis.localhost.get(uuid + "tpa");
            if (tpa != null) {
                Player target = Bukkit.getPlayerExact(tpa);
                player.teleport(target);
                Redis.localhost.del(uuid + "tpa");
            }
        }, 40L);
    }
}
