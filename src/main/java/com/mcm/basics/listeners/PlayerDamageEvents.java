package com.mcm.basics.listeners;

import com.mcm.basics.cache.GodHash;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.Arrays;

public class PlayerDamageEvents implements Listener {

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();

            if (GodHash.get() != null && Arrays.asList(GodHash.get().getList()).contains(player.getUniqueId().toString())) {
                event.setCancelled(true);
            }
        }
    }
}
