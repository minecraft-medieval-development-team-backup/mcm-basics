package com.mcm.basics.cache;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class PlayersVanish {

    private static final List<Player> players = new ArrayList<>();

    public static boolean isVanished(final Player player) {
        return PlayersVanish.players.contains(player);
    }

    public static boolean toggleVanish(final Player player) {
        if (PlayersVanish.isVanished(player))
            PlayersVanish.players.remove(player);
        else
            PlayersVanish.players.add(player);

        Bukkit.getOnlinePlayers().forEach(p -> {
            //if (!Main.scoreboardManager.hasSquadScoreboard(player))
            //    Main.scoreboardManager.getDefaultScoreboard().updateEntry(p, "online");
        });
        return PlayersVanish.isVanished(player);
    }

    public static List<Player> getPlayers() {
        return new ArrayList<>(PlayersVanish.players);
    }

}
