package com.mcm.basics.cache;

import java.util.HashMap;

public class LastPosition {

    private static String uuid;
    private static String location;
    private static HashMap<String, LastPosition> cache = new HashMap<>();

    public LastPosition (String uuid, String location) {
        this.uuid = uuid;
        this.location = location;
    }

    public LastPosition insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static LastPosition get(String uuid) {
        return cache.get(uuid);
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return this.location;
    }
}
