package com.mcm.basics.cache;

import java.util.ArrayList;
import java.util.HashMap;

public class GodHash {

    private static String name;
    private static ArrayList list;
    private static HashMap<String, GodHash> cache = new HashMap<>();

    public GodHash () {
        this.name = "server";
        this.list = new ArrayList();
    }

    public GodHash insert() {
        this.cache.put(this.name, this);
        return this;
    }

    public static GodHash get() {
        return cache.get("server");
    }

    public ArrayList getList() {
        return list;
    }

    public void addList(String uuid) {
        this.list.add(uuid);
    }

    public void removeList(String uuid) {
        this.list.remove(uuid);
    }
}
