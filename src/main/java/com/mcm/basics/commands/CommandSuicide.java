package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.Arrays;

public class CommandSuicide implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor", "moderador"};

        if (command.getName().equalsIgnoreCase("suicide") || command.getName().equalsIgnoreCase("suicidar")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 0) {
                    player.setHealth(0);
                    player.sendMessage(Main.getTradution("Df!2H@gG4%dDBxT", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_WOLF_DEATH, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    player.sendMessage(Main.getTradution("39mqN@%EH4JtM?x", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
