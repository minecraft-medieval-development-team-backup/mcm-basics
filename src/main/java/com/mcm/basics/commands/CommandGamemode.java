package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandGamemode implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};

        if (command.getName().equalsIgnoreCase("gamemode") || command.getName().equalsIgnoreCase("gm")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("sobrevivencia") || args[0].equalsIgnoreCase("sobrevivência")) {
                        player.setGameMode(GameMode.SURVIVAL);
                        player.sendMessage(Main.getTradution("9cHCwa*6hHZ48M7", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else if (args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("criativo")) {
                        player.setGameMode(GameMode.CREATIVE);
                        player.sendMessage(Main.getTradution("pk#vBvc#b256M2a", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else if (args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("adventure") || args[0].equalsIgnoreCase("aventura")) {
                        player.setGameMode(GameMode.ADVENTURE);
                        player.sendMessage(Main.getTradution("Fd$67JptQ63CmVW", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else if (args[0].equalsIgnoreCase("3") || args[0].equalsIgnoreCase("spectator") || args[0].equalsIgnoreCase("espectador")) {
                        player.setGameMode(GameMode.SPECTATOR);
                        player.sendMessage(Main.getTradution("tUskE$Jey!2genR", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(Main.getTradution("CR5#%Ws8bm2Hgjg", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (args.length == 2) {
                    Player target = Bukkit.getPlayerExact(args[1]);

                    if (target != null && target.isOnline()) {
                        if (args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("sobrevivencia") || args[0].equalsIgnoreCase("sobrevivência")) {
                            target.setGameMode(GameMode.SURVIVAL);
                            player.sendMessage(Main.getTradution("5mkS*QJGcjQw!63", uuid) + args[0] + Main.getTradution("Xqz$4!k64r@QgX*", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            target.sendMessage(Main.getTradution("a24F7xq7u?2pyNT", target.getUniqueId().toString()) + player.getName() + "!");
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else if (args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("criativo")) {
                            target.setGameMode(GameMode.CREATIVE);
                            player.sendMessage(Main.getTradution("5mkS*QJGcjQw!63", uuid) + args[0] + Main.getTradution("afNp5s&Dwkh$KWk", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            target.sendMessage(Main.getTradution("ybrM3x2ca&JThrx", target.getUniqueId().toString()) + player.getName() + "!");
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else if (args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("adventure") || args[0].equalsIgnoreCase("aventura")) {
                            target.setGameMode(GameMode.ADVENTURE);
                            player.sendMessage(Main.getTradution("5mkS*QJGcjQw!63", uuid) + args[0] + Main.getTradution("$t5v&8VJ3VDemVg", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            target.sendMessage(Main.getTradution("9?9kvHJ4zp@kxqv", target.getUniqueId().toString()) + player.getName() + "!");
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else if (args[0].equalsIgnoreCase("3") || args[0].equalsIgnoreCase("spectator") || args[0].equalsIgnoreCase("espectador")) {
                            target.setGameMode(GameMode.SPECTATOR);
                            player.sendMessage(Main.getTradution("5mkS*QJGcjQw!63", uuid) + args[0] + Main.getTradution("wWee$c7$eY4jU2H", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            target.sendMessage(Main.getTradution("e8&vd9UjtXg&g!w", target.getUniqueId().toString()) + player.getName() + "!");
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            target.sendMessage(Main.getTradution("CR5#%Ws8bm2Hgjg", target.getUniqueId().toString()));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("6MkuTqdhcWpn*D4", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("gH4RH5qk&*qpVS6", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
