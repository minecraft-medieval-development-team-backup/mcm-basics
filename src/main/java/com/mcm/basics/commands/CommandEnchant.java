package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class CommandEnchant implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};

        if (command.getName().equalsIgnoreCase("enchant") || command.getName().equalsIgnoreCase("encantar")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 2) {
                    ItemStack inHand = player.getItemInHand().clone();

                    if (inHand != null && !inHand.getType().equals(Material.AIR)) {
                        if (Enchantment.getByName(args[0].toUpperCase()) != null) {
                            Enchantment enchantment = Enchantment.getByName(args[0].toUpperCase());

                            try {
                                int level = Integer.valueOf(args[1]);

                                if (level > 0 && level <= enchantment.getMaxLevel()) {
                                    if (enchantment.canEnchantItem(inHand)) {
                                        inHand.addEnchantment(enchantment, level);
                                        player.setItemInHand(inHand);

                                        player.sendMessage(Main.getTradution("Nq4tGA!qRQk??4C", uuid));
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        player.playSound(player.getLocation(), Sound.BLOCK_ENCHANTMENT_TABLE_USE, 1.0f, 1.0f);
                                    } else {
                                        player.sendMessage(Main.getTradution("KjeWz65EdT3BZ!C", uuid));
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                } else {
                                    player.sendMessage(Main.getTradution("YS7#@*2qDveQU2F", uuid) + enchantment.getStartLevel() + Main.getTradution("Tv5mf9&**45HNqt", uuid) + enchantment.getMaxLevel() + ")!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            } catch (NumberFormatException e) {
                                player.sendMessage(Main.getTradution("%dutW@kAk78&p7%", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(Main.getTradution("JH?XJX9tZ@tFGx$", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("R&PwbMDX9QJ@ks*", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("%6zAFUxrp7zs38M", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }

        if (command.getName().equalsIgnoreCase("encantamentos") && Arrays.asList(perm).contains(tag)) {
            String enchants = null;
            for (Enchantment enchantment : Enchantment.values()) {
                if (enchants == null) {
                    enchants = enchantment.getName();
                } else enchants = enchants + ", " + enchantment.getName();
            }

            player.sendMessage(Main.getTradution("4QZ7hpZ4q#!daPr", uuid) + ChatColor.GRAY + enchants + ".");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
        return false;
    }
}
