package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.Arrays;

public class CommandKickall implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};

        if (command.getName().equalsIgnoreCase("kickall")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 0) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        if (com.mcm.core.database.TagDb.getTag(target.getUniqueId().toString()).equalsIgnoreCase("membro") || com.mcm.core.database.TagDb.getTag(target.getUniqueId().toString()).equalsIgnoreCase("vip") || com.mcm.core.database.TagDb.getTag(target.getUniqueId().toString()).equalsIgnoreCase("youtuber")) {
                            target.kickPlayer(Main.getTradution("h6qJG54Sc$7$bHv", target.getUniqueId().toString()));
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }
                } else {
                    player.sendMessage(Main.getTradution("qux#A&f2UnquGE4", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
