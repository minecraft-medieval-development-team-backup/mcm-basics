package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandWeather implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};

        if (command.getName().equalsIgnoreCase("weather") || command.getName().equalsIgnoreCase("clima")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("clear") || args[0].equalsIgnoreCase("limpar")) {
                        player.getWorld().setStorm(false);
                        player.getWorld().setThundering(false);

                        player.sendMessage(Main.getTradution("fGhp5%gk#9ShZ%H", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else if (args[0].equalsIgnoreCase("chuva") || args[0].equalsIgnoreCase("rain")) {
                        player.getWorld().setStorm(true);
                        player.getWorld().setThundering(true);

                        player.sendMessage(Main.getTradution("N@DE9?HMwkxdfT5", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("Huye5jPrkM&qE$9", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
