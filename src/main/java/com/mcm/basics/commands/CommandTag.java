package com.mcm.basics.commands;

import com.mcm.core.Main;
import com.mcm.basics.utils.TagsEnum;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.Arrays;

public class CommandTag implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        String[] perm = {"superior", "gestor"};
        String playerTag = com.mcm.core.database.TagDb.getTag(uuid);

        //tag setar (nick) (tag)
        if (command.getName().equalsIgnoreCase("tag")) {
            if (player.isOp() || Arrays.asList(perm).contains(playerTag)) {
                if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("setar")) {
                        Player target = Bukkit.getPlayerExact(args[1]);

                        if (target != null && target.isOnline()) {
                            //ERRO
                            if (TagsEnum.valueOf(args[2].toLowerCase()) != null) {
                                com.mcm.core.database.TagDb.updateTag(target.getUniqueId().toString(), args[2].toLowerCase());
                                player.sendMessage(Main.getTradution("3ajfUr2MjQ$%Vtv", uuid) + args[1]);
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } else {
                                player.sendMessage(Main.getTradution("JS6u&@xXCrpvpD%", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            }
                        } else {
                            player.sendMessage(Main.getTradution("6MkuTqdhcWpn*D4", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("QM%x!?yY76NM&G!", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("QM%x!?yY76NM&G!", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
