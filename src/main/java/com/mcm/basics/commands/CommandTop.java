package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandTop implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor", "moderador"};

        if (command.getName().equalsIgnoreCase("top") || command.getName().equalsIgnoreCase("topo")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 0) {
                    Location location = new Location(player.getWorld(), player.getLocation().getX(), player.getWorld().getHighestBlockYAt(player.getLocation()), player.getLocation().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch());
                    player.teleport(location);

                    player.sendMessage(Main.getTradution("sEa*5Sk&x3hYEJT", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                } else {
                    player.sendMessage(Main.getTradution("yE6E*B6QCva4m5Z", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
