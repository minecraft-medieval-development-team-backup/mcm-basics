package com.mcm.basics.commands;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mcm.core.Main;
import com.mcm.core.database.TagDb;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandConstruir implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] strings) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};
        if (command.getName().equalsIgnoreCase("construir") || command.getName().equalsIgnoreCase("builds") || command.getName().equalsIgnoreCase("builds")) {
            if (Arrays.asList(perm).contains(tag)) {
                try {
                    ByteArrayDataOutput out = ByteStreams.newDataOutput();
                    out.writeUTF("Connect");
                    out.writeUTF("builds");

                    player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());

                    player.sendMessage(Main.getTradution("wDcuNh4VjRY#k5c", uuid) + "builds.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                } catch (Exception e) {
                    player.sendMessage(Main.getTradution("yKx%z3mb5xnVVGs", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
