package com.mcm.basics.commands;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mcm.core.Main;
import com.mcm.core.RabbitMq;
import com.mcm.core.Redis;
import com.mcm.core.cache.PlayerLocation;
import com.mcm.core.cache.TpaExpired;
import com.mcm.core.cache.TpaInvitations;
import com.mcm.core.cache.TpaSended;
import com.mcm.core.database.RMqChatManager;
import com.mcm.core.enums.ServersList;
import com.mcm.core.serverswitch.PlayerSwitch;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandTeleport implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        if (command.getName().equalsIgnoreCase("tpa")) {
            if (args.length == 1) {
                String server = PlayerLocation.get(args[0]);

                if (server != null) {
                    if (TpaSended.get(uuid) != null && TpaSended.get(uuid).getList().contains(args[0])) {
                        player.sendMessage(Main.getTradution("RwJ65cjs?H?fKHP", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        return false;
                    }

                    player.sendMessage(Main.getTradution("?CAK2u#tkAB%MTv", uuid) + args[0] + ".");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    RabbitMq.send(server, RMqChatManager.key + "/tpa_solicite=" + args[0] + "=" + "Q#WnYUshT6jq!vr///" + player.getName() + "///63S@sFSctCP?$Bw///" + player.getName() + "=" + player.getName());
                    if (TpaSended.get(uuid) == null) new TpaSended(uuid).insert().addList(args[0]);
                    else TpaSended.get(uuid).addList(args[0]);
                } else {
                    player.sendMessage(Main.getTradution("6MkuTqdhcWpn*D4", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("aceitar") || args[0].equalsIgnoreCase("accept")) {
                    String server = PlayerLocation.get(args[1]);

                    if (server != null) {
                        if (TpaInvitations.get(player.getName()) != null && TpaInvitations.get(player.getName()).getList().contains(args[1])) {
                            TpaExpired.removeUser(player.getUniqueId().toString());
                            TpaInvitations.get(player.getName()).getList().remove(args[1]);

                            RabbitMq.send(server, RMqChatManager.key + "/tpaaccept=" + args[1] + "=" + Main.server_name + "=" + player.getName());

                            player.sendMessage(Main.getTradution("@cS5NzmA!@KS5Cv", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("5d2#px*6X3uzcX9", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("6MkuTqdhcWpn*D4", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (args[0].equalsIgnoreCase("negar") || args[0].equalsIgnoreCase("decline")) {
                    Player target = Bukkit.getPlayerExact(args[1]);

                    if (target != null && target.isOnline()) {
                        if (TpaInvitations.get(uuid) != null && TpaInvitations.get(uuid).getList().contains(target.getUniqueId().toString())) {
                            TpaExpired.removeUser(player.getUniqueId().toString());
                            TpaInvitations.get(uuid).getList().remove(target.getUniqueId().toString());

                            player.sendMessage(Main.getTradution("966ZPM5xNN&4pyN", uuid) + target.getName() + Main.getTradution("SwZRkv377DP6Yr@", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                            target.sendMessage(Main.getTradution("Kx%@mBNBcC3R35n", target.getUniqueId().toString()) + player.getName() + "!");
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            target.playSound(target.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("27TwFu?VFTF4kWp", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            } else {
                player.sendMessage(Main.getTradution("uB3?rDhV4kwnAH5", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }

        String[] permTphere = {"superior", "gestor", "moderador"};
        if (command.getName().equalsIgnoreCase("tphere") && Arrays.asList(permTphere).contains(tag)) {
            if (args.length == 1) {
                Player target = Bukkit.getPlayerExact(args[0]);

                if (target != null && target.isOnline()) {
                    target.teleport(player.getLocation());
                    target.sendMessage(Main.getTradution("fR9%Sbnj?7JXr*9", target.getUniqueId().toString()) + player.getName() + "!");
                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    target.playSound(target.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                } else {
                    player.sendMessage(Main.getTradution("6MkuTqdhcWpn*D4", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(Main.getTradution("bGt!c3p35t*p!ZR", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }

        String[] permTppos = {"superior", "gestor", "moderador"};
        if (command.getName().equalsIgnoreCase("tppos") && Arrays.asList(permTppos).contains(tag)) {
            if (args.length == 3) {
                try {
                    double x = Double.valueOf(args[0]);
                    double y = Double.valueOf(args[1]);
                    double z = Double.valueOf(args[2]);
                    Location location = new Location(player.getWorld(), x, y, z, player.getLocation().getYaw(), player.getLocation().getPitch());

                    player.teleport(location);
                    player.sendMessage(Main.getTradution("8wYHTCG9H?d8@SG", uuid) + player.getWorld().getName() + ", " + args[0] + ", " + args[1] + ", " + args[2] + ".");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } catch (NumberFormatException e) {
                    player.sendMessage(Main.getTradution("PKY6&pg7hVcb*@X", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(Main.getTradution("yPRV!FfG4RP5Wvz", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }

        String[] permTpall = {"superior", "gestor"};
        if (command.getName().equalsIgnoreCase("tpall") && Arrays.asList(permTpall).contains(tag)) {
            for (Player target : Bukkit.getOnlinePlayers()) {
                if (target.getName() != player.getName()) {
                    target.teleport(player.getLocation());
                    target.sendMessage(Main.getTradution("fR9%Sbnj?7JXr*9", target.getUniqueId().toString()) + player.getName() + "!");
                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    target.playSound(target.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                }
            }
        }

        String[] permTp = {"superior", "gestor", "moderador"};
        if (command.getName().equalsIgnoreCase("tp") && Arrays.asList(permTp).contains(tag)) {
            if (args.length == 1) {
                Player target = Bukkit.getPlayerExact(args[0]);

                if (target != null && target.isOnline()) {
                    player.teleport(target.getLocation());

                    player.sendMessage(Main.getTradution("fR9%Sbnj?7JXr*9", uuid) + target.getName());
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                } else {
                    String server = PlayerLocation.get(args[0]);
                    if (server != null) {
                        try {
                            ByteArrayDataOutput out = ByteStreams.newDataOutput();
                            out.writeUTF("Connect");
                            out.writeUTF(server);

                            player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());
                            player.sendMessage(Main.getTradution("fR9%Sbnj?7JXr*9", uuid) + target.getName());
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                            Redis.localhost.set(uuid + "tp", args[0]);
                            PlayerSwitch.sendData(player, server);
                        } catch (Exception e) {
                            player.sendMessage(Main.getTradution("y?C7uWPAkgs@hdv", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("6MkuTqdhcWpn*D4", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            } else if (args.length == 2) {
                Player one = Bukkit.getPlayerExact(args[0]);
                Player two = Bukkit.getPlayerExact(args[1]);

                if (one != null && one.isOnline() && two != null && two.isOnline()) {
                    one.teleport(two.getLocation());

                    one.sendMessage(Main.getTradution("fR9%Sbnj?7JXr*9", one.getUniqueId().toString()) + two.getName() + Main.getTradution("?dku#5!R!s%C@Hx", one.getUniqueId().toString()) + player.getName() + ".");
                    one.playSound(one.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    one.playSound(one.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                } else {
                    String player1 = PlayerLocation.get(args[0]);
                    String player2 = PlayerLocation.get(args[1]);

                    if (player1 != null && player2 != null) {
                        RabbitMq.send(player1, RMqChatManager.key + "/tp=" + args[0] + "=" + args[1]);
                    } else {
                        player.sendMessage(Main.getTradution("3aG!gtGe7q&@gv7", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                }
            } else {
                player.sendMessage(Main.getTradution("#WA2XHCRHTQp6pu", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
