package com.mcm.basics.commands;

import com.mcm.core.Main;
import com.mcm.basics.cache.GodHash;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandGod implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor", "moderador"};

        if (command.getName().equalsIgnoreCase("god") || command.getName().equalsIgnoreCase("deus")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 0) {
                    if (GodHash.get() == null) {
                        new GodHash().insert();
                    }

                    if (!GodHash.get().getList().contains(uuid)) {
                        GodHash.get().addList(uuid);
                        player.setInvulnerable(true);
                        player.sendMessage(Main.getTradution("8CrQeZGUum9w@w#", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.getWorld().strikeLightningEffect(player.getLocation());
                    } else {
                        GodHash.get().removeList(uuid);
                        player.setInvulnerable(false);
                        player.sendMessage(Main.getTradution("*fy6Yp23#nG%xR3", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.getWorld().strikeLightningEffect(player.getLocation());
                    }
                } else {
                    player.sendMessage(Main.getTradution("EQj4b%$YpmaRe5G", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
