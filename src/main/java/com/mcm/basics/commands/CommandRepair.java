package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class CommandRepair implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};

        if (command.getName().equalsIgnoreCase("repair") || command.getName().equalsIgnoreCase("reparar")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 0) {
                    ItemStack item = player.getItemInHand();
                    if (item != null && !item.getType().equals(Material.AIR)) {
                        item.setDurability((short) 0);
                        player.setItemInHand(item);

                        player.sendMessage(Main.getTradution("A%U?T6e%rs7*9xc", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(Main.getTradution("tPsNWfnH8s#Dc*s", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("8VgM2b5JG#Zc##6", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
