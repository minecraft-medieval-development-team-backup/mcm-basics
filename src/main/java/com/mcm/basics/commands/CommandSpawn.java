package com.mcm.basics.commands;

import com.mcm.core.Main;
import com.mcm.basics.managers.InventorySpawn;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandSpawn implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        if (command.getName().equalsIgnoreCase("spawn")) {
            player.openInventory(InventorySpawn.get(uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }

        String[] perm = {"superior", "gestor"};

        if (command.getName().equalsIgnoreCase("setspawn")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 0) {
                    if (com.mcm.core.database.SpawnDb.getSpawn() == null) {
                        String location = player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch();
                        com.mcm.core.database.SpawnDb.setSpawn(location);
                        player.sendMessage(Main.getTradution("$!XSRS8NcyX@3D6", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        String location = player.getWorld().getName() + ":" + player.getLocation().getX() + ":" + player.getLocation().getY() + ":" + player.getLocation().getZ() + ":" + player.getLocation().getYaw() + ":" + player.getLocation().getPitch();
                        com.mcm.core.database.SpawnDb.updateSpawn(location);
                        player.sendMessage(Main.getTradution("$6Yf$x8dNh?qq8f", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("E?AYtr8&8K%vy6D", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
