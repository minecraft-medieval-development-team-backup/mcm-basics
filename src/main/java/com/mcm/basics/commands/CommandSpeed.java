package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.Arrays;

public class CommandSpeed implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};

        if (command.getName().equalsIgnoreCase("speed") || command.getName().equalsIgnoreCase("velocidade")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 1) {
                    try {
                        int value = Integer.valueOf(args[0]);

                        if (value > 10 || value < 0) {
                            player.sendMessage(Main.getTradution("FJ@j54&s7&YkSxh", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            return false;
                        }

                        if (value == 0) {
                            if (player.isFlying()) {
                                player.setFlySpeed(0.0f);
                            } else {
                                player.setWalkSpeed(0.0f);
                            }
                            player.sendMessage(Main.getTradution("ANgrY2WDXgBw#UJ", uuid));
                        } else {
                            if (player.isFlying()) {
                                player.setFlySpeed(value * 0.1f);
                            } else {
                                player.setFlySpeed(value * 0.1f);
                            }
                            player.sendMessage(Main.getTradution("2Eb7RM3z3rp&taP", uuid) + value + ".");
                        }
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } catch (NumberFormatException e) {
                        player.sendMessage(Main.getTradution("FJ@j54&s7&YkSxh", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (args.length == 0) {
                    if (player.isFlying()) {
                        player.setFlySpeed(0.1f);
                    } else {
                        player.setFlySpeed(0.1f);
                    }
                    player.sendMessage(Main.getTradution("Yfvhj33EgTB3!bF", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    player.sendMessage(Main.getTradution("q@r$yyG5k27X&72", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
