package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Arrays;

public class CommandJump implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor", "moderador"};

        if (command.getName().equalsIgnoreCase("jump") || command.getName().equalsIgnoreCase("pular")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 1) {
                    try {
                        int potency = Integer.valueOf(args[0]);

                        if (potency > 0 && potency <= 10) {
                            Vector vector = player.getLocation().getDirection();
                            vector.setY(1 * potency);
                            player.setVelocity(vector);
                        } else {
                            player.sendMessage(Main.getTradution("*ZK$87&v5Zk%$8G", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    } catch (NumberFormatException e) {
                        player.sendMessage(Main.getTradution("2R$j35x69YfTVhR", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("Kk5QjdVwpvn&*zd", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
