package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandHeal implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor", "moderador"};

        if (command.getName().equalsIgnoreCase("heal")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 0) {
                    player.setHealth(20);
                    player.sendMessage(Main.getTradution("dX6hzTQQhF#ec&M", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else if (args.length == 1) {
                    Player target = Bukkit.getPlayerExact(args[0]);

                    if (target != null && target.isOnline()) {
                        target.setHealth(20);
                        target.sendMessage(Main.getTradution("E8QS7D!&hPF%Ch3", target.getUniqueId().toString()) + player.getName() + "!");
                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                        player.sendMessage(Main.getTradution("UW6cQW*8tpbjp$A", uuid) + args[0] + Main.getTradution("GM$BH3VCb4ymbT&", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(Main.getTradution("6MkuTqdhcWpn*D4", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("S&EwM#wcmUCf2J8", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
