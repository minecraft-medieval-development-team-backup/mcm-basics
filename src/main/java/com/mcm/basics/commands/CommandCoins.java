package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.text.DecimalFormat;
import java.util.Arrays;

public class CommandCoins implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        final DecimalFormat formatter = new DecimalFormat("#,###.00");

        String[] perm = {"superior", "gestor"};

        if (command.getName().equalsIgnoreCase("moeda") || command.getName().equalsIgnoreCase("coins")) {
            if (args.length == 3) {
                if (args[0].equalsIgnoreCase("setar") && Arrays.asList(perm).contains(tag)) {
                    Player target = Bukkit.getPlayerExact(args[1]);

                    if (target != null && target.isOnline()) {
                        try {
                            int value = Integer.valueOf(args[2]);

                            com.mcm.core.database.CoinsDb.updateCoins(target.getUniqueId().toString(), value);

                            player.sendMessage(Main.getTradution("b72vZ!yxxY3%!8K", uuid) + args[1] + Main.getTradution("8wYg4KKtSRFA68&", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } catch (NumberFormatException e) {
                            player.sendMessage(Main.getTradution("H%6D*97mdD9$mdH", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("6MkuTqdhcWpn*D4", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (args[0].equalsIgnoreCase("add") && Arrays.asList(perm).contains(tag)) {
                    Player target = Bukkit.getPlayerExact(args[1]);

                    if (target != null && target.isOnline()) {
                        try {
                            int valueNow = com.mcm.core.database.CoinsDb.getCoins(target.getUniqueId().toString());
                            int value = Integer.valueOf(args[2]);

                            com.mcm.core.database.CoinsDb.updateCoins(target.getUniqueId().toString(), valueNow + value);

                            player.sendMessage(Main.getTradution("b72vZ!yxxY3%!8K", uuid) + args[1] + Main.getTradution("8wYg4KKtSRFA68&", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } catch (NumberFormatException e) {
                            player.sendMessage(Main.getTradution("H%6D*97mdD9$mdH", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("6MkuTqdhcWpn*D4", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (args[0].equalsIgnoreCase("depositar") || args[0].equalsIgnoreCase("deposity")) {
                    Player target = Bukkit.getPlayerExact(args[1]);

                    if (target != null && target.isOnline()) {
                        try {
                            int valueNowTarget = com.mcm.core.database.CoinsDb.getCoins(target.getUniqueId().toString());
                            int value = Integer.valueOf(args[2]);
                            int valueNowPlayer = com.mcm.core.database.CoinsDb.getCoins(uuid);

                            if (value < 0) {
                                player.sendMessage(Main.getTradution("H%6D*97mdD9$mdH", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                return false;
                            }

                            if (valueNowPlayer >= value) {
                                com.mcm.core.database.CoinsDb.updateCoins(target.getUniqueId().toString(), valueNowTarget + value);
                                com.mcm.core.database.CoinsDb.updateCoins(player.getUniqueId().toString(), valueNowPlayer - value);

                                player.sendMessage(Main.getTradution("34**t?6Yf4W%D*E", uuid) + formatter.format(value) + Main.getTradution("SF%!k9Td6e$$Umk", uuid) + target.getName() + "!");
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                                target.sendMessage(Main.getTradution("DeJ#mww$H3$&Pq2", target.getUniqueId().toString()) + formatter.format(value) + Main.getTradution("88Gc&43uka$ZmCs", target.getUniqueId().toString()) + player.getName() + "!");
                                target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } else {
                                player.sendMessage(Main.getTradution("Uu3c#Xta3j!wTAy", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } catch (NumberFormatException e) {
                            player.sendMessage(Main.getTradution("H%6D*97mdD9$mdH", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("6MkuTqdhcWpn*D4", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("n&%YxcAn@YUQQ$4", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else if (args.length == 1) {
                Player target = Bukkit.getPlayerExact(args[0]);

                if (target != null && target.isOnline()) {
                    int coins = com.mcm.core.database.CoinsDb.getCoins(uuid);

                    player.sendMessage(ChatColor.YELLOW + " \n * " + args[0] + Main.getTradution("rzb7UBsY8#@FKAw", uuid) + formatter.format(coins) + Main.getTradution("ByHPWHE2%N5FX&B", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    player.sendMessage(Main.getTradution("6MkuTqdhcWpn*D4", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } else if (args.length == 0) {
                int coins = com.mcm.core.database.CoinsDb.getCoins(uuid);

                player.sendMessage(Main.getTradution("ESk?yP3X&6#*r!C", uuid) + formatter.format(coins) + Main.getTradution("ByHPWHE2%N5FX&B", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else {
                player.sendMessage(Main.getTradution("n&%YxcAn@YUQQ$4", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
