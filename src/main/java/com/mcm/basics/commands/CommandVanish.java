package com.mcm.basics.commands;

import com.mcm.core.Main;
import com.mcm.basics.cache.PlayersVanish;
import com.mcm.basics.utils.CentralizeMsg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandVanish implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor", "moderador"};

        if (command.getName().equalsIgnoreCase("vanish") || command.getName().equalsIgnoreCase("v")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 0) {
                    if (!PlayersVanish.isVanished(player)) {
                        PlayersVanish.toggleVanish(player);
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (!target.getName().equalsIgnoreCase(player.getName())) {
                                target.hidePlayer(Main.instance, player);
                            }
                        }
                        player.sendMessage(Main.getTradution("a7a%yUA%y8bfXP&", uuid));

                        player.setPlayerListHeaderFooter(ChatColor.translateAlternateColorCodes('&', " \n&6&lMinecraft Medieval\n&a  " + Main.getTradution("PbVnweJ3XmwR?uB", uuid) +"\n "), CentralizeMsg.sendCenteredMessage("\n                         " +
                                "&6Twitter: &fwww.twitter.com/mcmedieval                          " +
                                "\n&6Discord: &fbit.ly/McMedievalDiscord" +
                                "\n &6" + Main.getTradution("XP*dVd#BcC!z7kJ", uuid) + ": &fwww.minecraftmedieval.com\n "));

                        player.setGameMode(GameMode.SPECTATOR);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        PlayersVanish.toggleVanish(player);
                        for (final Player target : Bukkit.getOnlinePlayers()) {
                            if (!target.getName().equalsIgnoreCase(player.getName())) {
                                target.showPlayer(Main.instance, player);
                            }
                        }

                        player.setPlayerListHeaderFooter(ChatColor.translateAlternateColorCodes('&', " \n&6&lMinecraft Medieval\n "), CentralizeMsg.sendCenteredMessage("\n                         " +
                                "&6Twitter: &fwww.twitter.com/mcmedieval                          " +
                                "\n&6Discord: &fbit.ly/McMedievalDiscord" +
                                "\n &6" + Main.getTradution("XP*dVd#BcC!z7kJ", uuid) + ": &fwww.minecraftmedieval.com\n "));

                        player.sendMessage(ChatColor.GREEN + " * Vanish desabilitado!");

                        player.setGameMode(GameMode.SURVIVAL);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("qH5#bNMgvyWdex%", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
