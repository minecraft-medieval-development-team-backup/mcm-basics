package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.Arrays;

public class CommandTime implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};

        if (command.getName().equalsIgnoreCase("time") || command.getName().equalsIgnoreCase("tempo")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 2) {
                    if (args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("setar")) {
                        if (args[1].equalsIgnoreCase("day") || args[1].equalsIgnoreCase("dia")) {
                            player.getWorld().setTime(0);
                            player.sendMessage(Main.getTradution("t26*eVpa32?Rr?U", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else if (args[1].equalsIgnoreCase("night") || args[1].equalsIgnoreCase("noite")) {
                            player.getWorld().setTime(17000);
                            player.sendMessage(Main.getTradution("B@Q!e8C9d6&AJ8J", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            try {
                                int time = Integer.valueOf(args[1]);
                                player.getWorld().setTime(time);
                                player.sendMessage(Main.getTradution("$6wy*Bu4PkE8MXX", uuid) + args[1]);
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            } catch (NumberFormatException e) {
                                player.sendMessage(Main.getTradution("8K*m!qex6B3D*!N", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        }
                    } else {
                        player.sendMessage(Main.getTradution("8K*m!qex6B3D*!N", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("8K*m!qex6B3D*!N", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
