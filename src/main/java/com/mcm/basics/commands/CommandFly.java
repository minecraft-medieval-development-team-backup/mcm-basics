package com.mcm.basics.commands;

import com.mcm.core.Main;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.Arrays;

public class CommandFly implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor", "moderador", "suporte", "vip", "youtuber"};
        if (command.getName().equalsIgnoreCase("fly") || command.getName().equalsIgnoreCase("voar")) {
            if (args.length == 0) {
                if (Arrays.asList(perm).contains(tag)) {
                    if (player.getAllowFlight() == false) {
                        player.setAllowFlight(true);
                        player.sendMessage(Main.getTradution("hS2@&c8CgzWyW$H", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        player.setAllowFlight(false);
                        player.sendMessage(Main.getTradution("#svagWk4?Ykvzq?", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("BBHB@Z3?Dcjh!Tw", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            } else {
                player.sendMessage(Main.getTradution("6vegn&9sXsqHWhc", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
        return false;
    }
}
